import ECharts from 'vue-echarts/components/ECharts'

import chinaMap from '../map/china'
import anhui from '../map/anhui'
import aomen from '../map/aomen'
import beijing from '../map/beijing'
import chongqing from '../map/chongqing'
import fujian from '../map/fujian'
import gansu from '../map/gansu'
import guangdong from '../map/guangdong'
import guangxi from '../map/guangxi'
import guizhou from '../map/guizhou'
import hainan from '../map/hainan'
import hebei from '../map/hebei'
import heilongjiang from '../map/heilongjiang'
import henan from '../map/henan'
import hubei from '../map/hubei'
import hunan from '../map/hunan'
import jiangsu from '../map/jiangsu'
import jiangxi from '../map/jiangxi'
import jilin from '../map/jilin'
import liaoning from '../map/liaoning'
import neimenggu from '../map/neimenggu'
import ningxia from '../map/ningxia'
import qinghai from '../map/qinghai'
import shandong from '../map/shandong'
import shanghai from '../map/shanghai'
import shanxi from '../map/shanxi'
import sichuan from '../map/sichuan'
import taiwan from '../map/taiwan'
import tianjin from '../map/tianjin'
import hongkong from '../map/hongkong'
import xizang from '../map/xizang'
import yunnan from '../map/yunnan'
import zhejiang from '../map/zhejiang'
import xinjiang from '../map/xinjiang'
import shanxi1 from '../map/shanxi1'

ECharts.registerMap('china', chinaMap)
ECharts.registerMap('安徽省', anhui)
ECharts.registerMap('澳门特别行政区', aomen)
ECharts.registerMap('北京市', beijing)
ECharts.registerMap('重庆市', chongqing)
ECharts.registerMap('福建省', fujian)
ECharts.registerMap('甘肃省', gansu)
ECharts.registerMap('广东省', guangdong)
ECharts.registerMap('广西壮族自治区', guangxi)
ECharts.registerMap('贵州省', guizhou)
ECharts.registerMap('海南省', hainan)
ECharts.registerMap('河北省', hebei)
ECharts.registerMap('黑龙江省', heilongjiang)
ECharts.registerMap('河南省', henan)
ECharts.registerMap('湖北省', hubei)
ECharts.registerMap('湖南省', hunan)
ECharts.registerMap('江苏省', jiangsu)
ECharts.registerMap('江西省', jiangxi)
ECharts.registerMap('吉林省', jilin)
ECharts.registerMap('辽宁省', liaoning)
ECharts.registerMap('内蒙古自治区', neimenggu)
ECharts.registerMap('宁夏回族自治区', ningxia)
ECharts.registerMap('青海省', qinghai)
ECharts.registerMap('山东省', shandong)
ECharts.registerMap('上海市', shanghai)
ECharts.registerMap('山西省', shanxi)
ECharts.registerMap('四川省', sichuan)
ECharts.registerMap('台湾省', taiwan)
ECharts.registerMap('天津市', tianjin)
ECharts.registerMap('香港特别行政区', hongkong)
ECharts.registerMap('西藏自治区', xizang)
ECharts.registerMap('云南省', yunnan)
ECharts.registerMap('浙江省', zhejiang)
ECharts.registerMap('新疆维吾尔自治区', xinjiang)
ECharts.registerMap('陕西省', shanxi1)

const GlobalComponents = {}

GlobalComponents.install = (Vue) => {
  Vue.prototype.theme = 'darkTheme'
  Vue.component('echarts', ECharts)
}

export default GlobalComponents
