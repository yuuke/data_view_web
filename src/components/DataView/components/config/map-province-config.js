const MapProvinceConfig = function() {
  this.config = {
    x: 0,
    y: 0,
    width: 350,
    height: 350,
    chartType: 'mapProvince',
    choose: 'false',
    refresh: 'false',
    chartData: {
      dataSourceType: '',
      database: '',
      fileName: '',
      sql: 'select * from map_province_hubei',
      name: 'name',
      value: 'value',
      province: '湖北省'
    },
    data: [],
    interval: 8000,
    option: {
      title: {
        text: '省份地图',
        left: 'center',
        textStyle: {
          color: '#fff'
        }
      },
      grid: {
        top: '60',
        left: '1%',
        right: '4%',
        bottom: '10%',
        containLabel: true
      },
      visualMap: {
        min: 0,
        max: 0,
        left: '10%',
        top: 'bottom',
        calculable: true,
        seriesIndex: [0],
        inRange: {
          color: ['#04387b', '#467bc0'] // 蓝绿
        },
        textStyle: {
          color: '#fff'
        }
      },
      tooltip: {
        formatter: ''
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      geo: {
        show: true,
        map: '',
        label: {
          normal: {
            show: true,
            textStyle: {
              color: '#fff'
            }
          },
          emphasis: {
            show: true,
            textStyle: {
              color: '#023677'
            }
          }
        },
        roam: false,
        itemStyle: {
          normal: {
            areaColor: '#023677',
            borderColor: '#1180c7'
          },
          emphasis: {
            areaColor: '#4499d0'
          }
        }
      },
      series: [
        {
          type: 'map',
          map: '',
          geoIndex: 0,
          aspectScale: 0.75, // 长宽比
          showLegendSymbol: false, // 存在legend时显示
          label: {
            normal: {
              show: true
            },
            emphasis: {
              show: false,
              textStyle: {
                color: '#fff'
              }
            }
          },
          roam: true,
          itemStyle: {
            normal: {
              areaColor: '#031525',
              borderColor: '#3B5077'
            },
            emphasis: {
              areaColor: '#2B91B7'
            }
          },
          animation: false,
          data: []
        }
      ],
      backgroundColor: 'rgba(255,255,255,0)'
    }
  }
}

const getMapProvinceConfig = function() {
  return new MapProvinceConfig().config
}

export { getMapProvinceConfig }
