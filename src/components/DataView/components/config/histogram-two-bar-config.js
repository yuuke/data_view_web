const HistogramTwoBarConfig = function() {
  this.config = {
    x: 0,
    y: 0,
    width: 350,
    height: 250,
    chartType: 'histogramTwoBar',
    choose: 'false',
    refresh: 'false',
    chartData: {
      dataSourceType: '',
      database: '',
      fileName: '',
      sql: 'select * from histogram_two_bar',
      x: 'x',
      y: 'y',
      legend: 'legend'
    },
    data: {},
    interval: 8000,
    option: {
      title: { text: '双向柱状图', left: 'center', textStyle: { color: '#fff' }},
      tooltip: {
        trigger: 'axis',
        axisPointer: { // 坐标轴指示器，坐标轴触发有效
          type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
        },
        //     formatter: function(params){return Math.max(params.value,-params.value)}

        formatter: function(params, ticket, callback) {
          console.log(params)
          var res = params[0].name
          for (var i = 0, l = params.length; i < l; i++) {
            res += '<br/>' + params[i].seriesName + ' : ' + Math.abs(params[i].value)
          }
          return res
        }
      },
      legend: {
        data: [],
        left: 'right',
        textStyle: { color: '#fff' }
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: {
        name: '',
        type: 'value',
        axisLabel: {
          show: true,
          textStyle: { color: '#fff' },
          formatter: function(value) {
            return Math.abs(value)// 显示的数值都取绝对值
          }
        }, // 分类文字的显示及颜色
        axisTick: { show: false }, // 轴上的线，一般不用，设置为false
        axisLine: { show: true, lineStyle: { color: '#ffffff' }}, // 轴线
        splitLine: { show: true, lineStyle: { color: '#ffffff' }} // 图上的分隔线
      },
      yAxis: {
        name: '',
        type: 'category',
        data: [],
        axisLabel: { show: true, textStyle: { color: '#fff' }}, // 分类文字的显示及颜色
        axisTick: { show: false }, // 轴上的线，一般不用，设置为false
        axisLine: { show: true, lineStyle: { color: '#ffffff' }}, // 轴线
        splitLine: { show: true, lineStyle: { color: '#ffffff' }, type: 'dashed' }// 图上的分隔线
      },
      series: [

        {
          name: '',
          type: 'bar',
          stack: '总量',
          label: {
            normal: {
              show: true
            }
          },
          data: []
        },
        {
          name: '',
          type: 'bar',
          stack: '总量',
          label: {
            normal: {
              show: true,
              formatter: function(params) { return -params.value }
            }
          },
          data: []
        }
      ],
      backgroundColor: 'rgba(255,255,255,0)'
    }
  }
}

const getHistogramTwoBarConfig = function() {
  return new HistogramTwoBarConfig().config
}

export { getHistogramTwoBarConfig }
