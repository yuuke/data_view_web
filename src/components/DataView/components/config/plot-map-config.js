const PlotMapConfig = function() {
  this.config = {
    x: 0,
    y: 0,
    width: 350,
    height: 250,
    chartType: 'plotMap',
    choose: 'false',
    refresh: 'false',
    chartData: {
      dataSourceType: '',
      database: '',
      fileName: '',
      sql: 'select * from scatter_map_1',
      name: 'name',
      value: 'value',
      legend: 'legend'
    },
    data: [],
    interval: 8000,
    option: {
      showDetail: '0',
      title: {
        text: '散点地图',
        left: 'center',
        top: 'top',
        textStyle: {
          color: '#fff'
        }
      },
      tooltip: {
        trigger: 'item',
        formatter: function(params) {
          if (params.value[2]) {
            return params.name + ' : ' + params.value[2]
          }
        }
      },
      visualMap: {
        show: true,
        min: 0,
        max: 0,
        left: '10%',
        top: 'bottom',
        calculable: true,
        seriesIndex: [1],
        inRange: {
          color: ['#04387b', '#467bc0'] // 蓝绿
        },
        textStyle: {
          color: '#fff'
        }
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      geo: {
        show: true,
        map: 'china',
        label: {
          normal: {
            show: false
          },
          emphasis: {
            show: false
          }
        },
        roam: false,
        itemStyle: {
          normal: {
            areaColor: '#023677',
            borderColor: '#1180c7'
          },
          emphasis: {
            areaColor: '#4499d0'
          }
        }
      },
      series: [
        {
          name: '散点',
          type: 'scatter',
          coordinateSystem: 'geo',
          data: [],
          symbolSize: 10,
          label: {
            normal: {
              formatter: '{b}',
              position: 'right',
              show: true
            },
            emphasis: {
              show: true
            }
          },
          itemStyle: {
            normal: {
              color: '#fff'
            }
          }
        },
        {
          type: 'map',
          map: 'china',
          geoIndex: 0,
          aspectScale: 0.75, // 长宽比
          showLegendSymbol: false, // 存在legend时显示
          label: {
            normal: {
              show: true
            },
            emphasis: {
              show: false,
              textStyle: {
                color: '#fff'
              }
            }
          },
          roam: true,
          itemStyle: {
            normal: {
              areaColor: '#031525',
              borderColor: '#3B5077'
            },
            emphasis: {
              areaColor: '#2B91B7'
            }
          },
          animation: false,
          data: []
        },
        {
          name: '点',
          type: 'scatter',
          coordinateSystem: 'geo',
          zlevel: 6
        },
        {
          name: 'Top 3',
          type: 'effectScatter',
          coordinateSystem: 'geo',
          data: [],
          symbolSize: 15,
          showEffectOn: 'render',
          rippleEffect: {
            brushType: 'stroke'
          },
          hoverAnimation: true,
          label: {
            normal: {
              formatter: '{b}',
              position: 'left',
              show: false
            }
          },
          itemStyle: {
            normal: {
              color: '#ffff00',
              shadowBlur: 10,
              shadowColor: '#ffff00'
            }
          },
          zlevel: 1
        }
      ],
      backgroundColor: 'rgba(255,255,255,0)'
    }
  }
}

const getPlotMapConfig = function() {
  return new PlotMapConfig().config
}

export { getPlotMapConfig }
